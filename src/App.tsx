import { timeStamp } from 'console';
import { 
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
  signInWithEmailAndPassword,
 } from 'firebase/auth';
 
 import * as firebase from 'firebase/app';

import { 
  getFirestore, 
  getDocs, 
  collection, 
  addDoc, 
  doc,
  deleteDoc, 
  onSnapshot,
  serverTimestamp,
  updateDoc,
  query,
  where
} from 'firebase/firestore';
import React, { useState, useEffect } from 'react';
import { FormCheck } from "react-bootstrap"
import { auth, db } from './firebase';


interface UserDetails{
  uid: string;
  email: string;
};

export type TaskTypes = {
  id: string;
  title: string;
  done: boolean;
  userId: string;
}

const taskCollectionRef = collection(db, 'tasks');



function App() {

  const [registerEmail, setRegisterEmail]= useState("");
  const [registerPassword, setRegisterPassword]= useState("");
  const [loginEmail, setLoginEmail]= useState("");
  const [loginPassword, setLoginPassword]= useState("");
  const [title, setTitle] = useState("");
  const [tasks, setTasks] = useState<TaskTypes[]>([]);
  const [user, setUser] = useState<UserDetails | null>(null);


  useEffect( () =>{
    const unsubscribe = auth.onAuthStateChanged((authUser) => {
      if (authUser !== null){
        const userDetails: UserDetails = {
          uid: authUser!.uid,
          email: authUser!.email ?? "SEM EMAIL"
        };
        setUser(userDetails);
      }else{
        setUser(null);
      }
      console.log("Auth State:",authUser);
    })
    return function cleanup(){
      console.log('cleaningup'); 
      unsubscribe();
    };
  },[]);

  const registerUser = async () => {
    const user = await createUserWithEmailAndPassword(auth, registerEmail , registerPassword);
    console.log(user);
  };
  const login = async () => {
    const user = await signInWithEmailAndPassword(auth, loginEmail, loginPassword);
     
    console.log(user);
  };
  const logout = async () => {
    await signOut(auth)
  };
  console.log("currentUser: ", user);

  async function createTask() {
    const task = await addDoc(taskCollectionRef, {
      title,
      done: false,
      userId: user?.uid,
      createdAt: serverTimestamp()
    });
    console.log(task);
  };

  const deleteTask = async (id: string) => {
    const taskDeleted = doc(db, 'tasks', id);
    await deleteDoc(taskDeleted);
  }

  const doneUpdate = async (task: TaskTypes) => {
    const taskChanged = doc(db, 'tasks', task.id);
    await updateDoc(taskChanged, {
      done: !task.done,
    });
    console.log("task status: ", task.done);
  }

  useEffect(() => {
    if (user){
      let taskList: TaskTypes[] = [];
      const userTaskCollectionRef = query(taskCollectionRef, where('userId', '==', user?.uid ));
      const showTasks = onSnapshot(userTaskCollectionRef, (querySnapshot: { docs: any[]; }) => {
        querySnapshot.docs.map((doc) => {
          const task: TaskTypes={
            id: doc.data().id,
            title: doc.data().title,
            done: doc.data().done,
            userId: doc.data().userId,
          };
          taskList.push(task);
        });
        setTasks(taskList);
      });
      return showTasks;
    } else {
      setTasks([]);
    } 
  },[user, tasks]);

  return (
    <div>
      <div>
        <h3>Register User</h3>
        <input placeholder="Email..." onChange={(e) => {
          setRegisterEmail(e.target.value);
        }}/>
        <input placeholder="Password..." onChange={(e) => {
          setRegisterPassword(e.target.value);
        }}/>
        <button onClick={registerUser}>Create user</button>
      </div>
      <div>
        <h3>Login</h3>
        <input placeholder="Email..." onChange={(e) => {
          setLoginEmail(e.target.value);
        }}/>
        <input placeholder="Password..." onChange={(e) => {
          setLoginPassword(e.target.value);
        }}/>
        <button onClick={login}>Login</button>
        <br/>
      </div>
      <div>
            <input type="text" placeholder='Task' value={title} onChange={e => setTitle(e.target.value)}/>
            <button onClick={createTask}>Criar task</button>
            <ul>
                {tasks?.map((task, index) => {
                    return (
                      <>
                        <FormCheck
                          key={index}
                          checked={task.done}
                          onChange={() => doneUpdate(task)}
                          label={task.title} />
                        <button onClick={() => deleteTask(task.id)}>Apagar Task</button>
                      </>
                    )
                })}
            </ul>
        </div>
      <h4>User Logged in: </h4>
      {user?.email}
      <button onClick={logout}>Sign Out</button>
    </div>
  );
}

export default App;



import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCqd5BKHAIw0qWmyfrRLRAmfnWztOwfTd0",
  authDomain: "teste-5c6df.firebaseapp.com",
  projectId: "teste-5c6df",
  storageBucket: "teste-5c6df.appspot.com",
  messagingSenderId: "129583986770",
  appId: "1:129583986770:web:8ebbb1a7db146d77a6478b",
  measurementId: "G-29C3SWNC01"

  /*
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID,
  appId: process.env.REACT_APP_FIREBASE_APP_ID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
  */
  
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);

